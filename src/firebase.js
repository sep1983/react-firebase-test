// src/firebase.js
import firebase from 'firebase'
const config = {
    apiKey: "AIzaSyDPubYPG63L7wtuD2w1zyW4JPFPBGODeWU",
    authDomain: "fun-test-bdcaa.firebaseapp.com",
    databaseURL: "https://fun-test-bdcaa.firebaseio.com",
    projectId: "fun-test-bdcaa",
    storageBucket: "fun-test-bdcaa.appspot.com",
    messagingSenderId: "729956996743",
    appId: "1:729956996743:web:7f74bd2dc5a29c0bf10a22"
};
firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;
